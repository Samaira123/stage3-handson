import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;

public class Solution {

    // Complete the miniMaxSum function below.
    static void miniMaxSum(int[] arr) {
        
        long t=0L;
        
        for(int i=0; i<arr.length; i++){
            t += arr[i];
        }
        long max=-99999999999999L;
        long min=9999999999999L;
        for(int i=0; i<arr.length; i++){
            long flag = t-arr[i];
            if(max < flag)
            {
                max = flag;
            }
            if(min > flag){
                min = flag;
            }
        }
        System.out.println(min+" "+max);

    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        int[] arr = new int[5];

        String[] arrItems = scanner.nextLine().split(" ");
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        for (int i = 0; i < 5; i++) {
            int arrItem = Integer.parseInt(arrItems[i]);
            arr[i] = arrItem;
        }

        miniMaxSum(arr);

        scanner.close();
    }
}
